# [Programming With Sheba](https://git.mpcreation.net/software-house/klabisz-michal/programming-with-sheba)

Programming With Sheba, is a VSCode extension that shows you Sheba poses that correlate to the number of errors in your code!

![Loading...](https://babel.stronazen.pl/sheba/sheba.gif)

Extension based on [In Your Face](https://github.com/virejdasani/InYourFace/) which is based on doom 'Ouch Faces'
